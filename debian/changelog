ruby-net-ssh-multi (1.2.1-6) experimental; urgency=medium

  * Team upload.
  * Remove XB-Ruby-Versions (Not needed)
  * Bump Standards-Version to 4.7.0 (no changes needed)
  * Add patch to fix FTBFS with mocha 2 (Closes: #1090871)

 -- Vivek K J <vivekkj@disroot.org>  Mon, 23 Dec 2024 22:19:30 +0530

ruby-net-ssh-multi (1.2.1-5) UNRELEASED; urgency=medium

  * Update standards version to 4.6.0, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 14 Apr 2022 21:32:36 -0000

ruby-net-ssh-multi (1.2.1-4) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Update watch file format version to 4.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-mocha, ruby-net-ssh and
      ruby-net-ssh-gateway.
    + ruby-net-ssh-multi: Drop versioned constraint on ruby-net-ssh and
      ruby-net-ssh-gateway in Depends.

  [ Lucas Nussbaum ]
  * Refresh packaging using dh-make-ruby -w

 -- Lucas Nussbaum <lucas@debian.org>  Thu, 14 Apr 2022 20:29:38 +0200

ruby-net-ssh-multi (1.2.1-3) unstable; urgency=medium

  * Team upload
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Disable tests failing with Mocha 1.2 due to added visibility checks
  * Bump Standards-Version to 4.0.0 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Sat, 08 Jul 2017 17:02:05 +0200

ruby-net-ssh-multi (1.2.1-2) unstable; urgency=medium

  * Team upload.
  * require-timeout.patch : Explicitly require timeout. (Closes: #811462)
  * Bump debhelper compatibility.

 -- Balasankar C <balasankarc@autistici.org>  Thu, 28 Jan 2016 00:33:52 +0530

ruby-net-ssh-multi (1.2.1-1) unstable; urgency=medium

  * New upstream release.
  * Upload to unstable now that jessie is released.
  * Drop patch: require_mocha_setup.patch. Merged upstream.
  * ruby-tests.rake: set t.libs.

 -- Lucas Nussbaum <lucas@debian.org>  Tue, 05 May 2015 13:15:49 +0200

ruby-net-ssh-multi (1.2.0-3~exp1) experimental; urgency=medium

  * Team upload.

  [ Lucas Nussbaum ]
  * Remove myself from Uploaders.

  [ Sebastien Badia ]
  * Target experimental and build with ruby 2.2.
  * Add ruby-test-unit to Build-Depends for ruby2.2
  * Bump Standards-Version to 3.9.6 (no further changes)
  * Update Vcs-Browser to cgit URL and HTTPS
  * Fix lintian issue (no-dep5-copyright)

 -- Sebastien Badia <seb@sebian.fr>  Fri, 10 Apr 2015 18:26:52 +0200

ruby-net-ssh-multi (1.2.0-2) unstable; urgency=medium

  * Team upload.
  * Add require_mocha_setup.patch:
    require mocha/setup instead of mocha, which is the way to ensure recent
    versions of Mocha are currently initialized.
    This fix test failures with Mocha 1.0 (Closes: #741769)
  * Add a minimal 0.13~ version for ruby-mocha in build-dependencies.

 -- Cédric Boutillier <boutil@debian.org>  Sun, 16 Mar 2014 23:46:05 +0100

ruby-net-ssh-multi (1.2.0-1) unstable; urgency=low

  * Team upload.

  [ Cédric Boutillier ]
  * debian/control: remove obsolete DM-Upload-Allowed flag
  * use canonical URI in Vcs-* fields

  [ Jonas Genannt ]
  * Imported Upstream version 1.2.0
  * d/control:
    - removed transitional packages
    - bumped standards version to 3.9.5 (no changes needed)
    - changed ruby1.8 dependency to ruby
    - wrap-sort

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Sun, 01 Dec 2013 20:00:54 +0100

ruby-net-ssh-multi (1.1-2) unstable; urgency=low

  * Team upload.
  * debian/control:
    - Bump build dependency on gem2deb to >= 0.3.0~.
    - Bumped the standards-version to 3.9.3.
    - Set the priority of the transitional packages to "extra".
    - Replaced conflicts by breaks.

 -- Paul van Tilburg <paulvt@debian.org>  Wed, 27 Jun 2012 21:55:58 +0200

ruby-net-ssh-multi (1.1-1) unstable; urgency=low

  * Switch to gem2deb-based packaging, rename source and binary package.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Sat, 16 Apr 2011 09:56:28 +0200

libnet-ssh-multi-ruby (1.0.1-1) unstable; urgency=low

  * Initial release. (Closes: #574846)

 -- Joshua Timberman <joshua@opscode.com>  Sun, 21 Mar 2010 10:49:04 -0600
